module.exports = function(grunt) {

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    compass: {
      dev: {
        options: {
          config: 'config.rb',
          force: true
        }
      }
    },

    concat: {
        library: {
            src: 'javascripts/library/*.js',
            dest: 'javascripts/libraries.min.js'
        },
        app: {
            src: 'javascripts/app/*.js',
            dest: 'javascripts/application.min.js'
        }
    },
        
    uglify: {
      my_target: {
        files: {
          'build.min.js': ['javascripts/**/*.js']
        }
      }
    },

    cssmin: {
      combine: {
        files: {
          'stylesheets/screen.css': ['stylesheets/screen.css']
        }
      }
    },       
    
    watch: {
      sass: {
        files: ['sass/**/*.scss'],
        tasks: ['compass:dev'],
        option: {
          livereload: true 
        }        
      },
      css: {
        files: 'stylesheets/screen.css',
        option: {
          livereload: true 
        }
      },
      /* watch and see if our javascript files change, or new packages are installed */
      js: {
        files: ['javascripts/**/*.js'],
        tasks: ['concat'],
        dest: 'build.js'
      },
      /* watch our files for change, reload */
      livereload: {
        files: ['**/*.html', '**/*.php', 'stylesheets/*.css', 'images/*'],
        options: {
          livereload: true
        }
      },
    }
    
  });

  
  /* Grunt Tasks */
  grunt.registerTask('build', [
    'cssmin',
    'concat'
  ]);

  grunt.registerTask('default', [ 
    'watch'
  ]);   


};
