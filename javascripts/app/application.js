$(document).ready(function(){
	
	// IE image scaling
	imgSizer.collate();
	
	// automatically adjust textarea height
	$('textarea').autosize({append: "\n"});

}); // $(document).ready